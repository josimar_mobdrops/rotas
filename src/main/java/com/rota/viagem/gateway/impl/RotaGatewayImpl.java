package com.rota.viagem.gateway.impl;

import com.rota.viagem.gateway.RotaGateway;
import com.rota.viagem.gateway.data.request.RotaGatewayRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;

@Log
@Component
@RequiredArgsConstructor
public class RotaGatewayImpl implements RotaGateway {


    @Override
    public void inserirRegistroArquivo(RotaGatewayRequest gatewayRequest) throws IOException {
        String path = System.getProperty("input.file.name");
        final boolean hasPath = (StringUtils.isEmpty(path));
        final boolean hasFilePath = (!Paths.get(path).toFile().exists());
        if(hasPath){
            log.severe("ERRO: Variável -Dinput.file.name não definida");
            throw new IllegalArgumentException("Path Not Found!");
        }else if(hasFilePath){
            log.severe("ERRO: Arquivo não encontrado!");
            throw new IllegalArgumentException("File Not Found!");
        }
            File file = Paths.get(path).toFile();

            FileWriter writer = new FileWriter(file, true);
            writer.write("\n");
            writer.write(gatewayRequest.getRotaDe() + ",");
            writer.write(gatewayRequest.getRotaPara() + ",");
            writer.write(String.valueOf(gatewayRequest.getValor()));
            writer.flush();
            writer.close();
    }
}
