package com.rota.viagem.gateway.impl;

import com.rota.viagem.converter.RotaConverter;
import com.rota.viagem.entity.ArquivoEntity;
import com.rota.viagem.gateway.RotaConsultaGateway;
import com.rota.viagem.gateway.data.request.RotaConsultaGatewayRequest;
import com.rota.viagem.gateway.data.response.ResultadoRotaGateway;
import com.rota.viagem.gateway.data.response.RotaConsultaGatewayResponse;
import com.rota.viagem.usecase.impl.Graph;
import com.rota.viagem.utils.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Log
@Component
@RequiredArgsConstructor

public class RotaConsultaGatewayImpl implements RotaConsultaGateway {

    private final RotaConverter converter;

    @Override
    public RotaConsultaGatewayResponse executar(RotaConsultaGatewayRequest gatewayRequest, List<ArquivoEntity> arquivoEntityList) throws IOException {

        Graph aeroporto = new Graph(arquivoEntityList.size(), 6);

        arquivoEntityList.stream()
                .filter(Objects::nonNull)
                .forEach(rota -> aeroporto.tracarEscala(
                        Utils.converterStringParaInteger(rota.getOrigemRota()),
                        Utils.converterStringParaInteger(rota.getDestinoRota()),
                        Integer.valueOf(rota.getValor())));
        ResultadoRotaGateway resultadoTrafego = aeroporto.path(gatewayRequest.getPartida(), gatewayRequest.getDestino());

        return converter.converterRotaConsultaGatewayToRotaConsultaResponse(resultadoTrafego);
    }
}
