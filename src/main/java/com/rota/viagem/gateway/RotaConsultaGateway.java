package com.rota.viagem.gateway;

import com.rota.viagem.controller.data.response.RotaConsultaResponse;
import com.rota.viagem.entity.ArquivoEntity;
import com.rota.viagem.gateway.data.request.RotaConsultaGatewayRequest;
import com.rota.viagem.gateway.data.response.RotaConsultaGatewayResponse;

import java.io.IOException;
import java.util.List;

public interface RotaConsultaGateway {

    RotaConsultaGatewayResponse executar(RotaConsultaGatewayRequest gatewarRequest, List<ArquivoEntity> arquivoEntityList) throws IOException;
}
