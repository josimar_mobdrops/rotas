package com.rota.viagem.gateway.data.request;

import lombok.*;

import java.math.BigDecimal;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RotaGatewayRequest {

    private String rotaDe;
    private String rotaPara;
    private BigDecimal valor;
}
