package com.rota.viagem.gateway.data.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class RotaConsultaGatewayRequest {

    private int partida;
    private int destino;
}
