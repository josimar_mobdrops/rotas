package com.rota.viagem.gateway.data.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Builder
@Getter
public class RotaConsultaGatewayResponse {

    private List<String> caminho;
    private Integer valor;
}
