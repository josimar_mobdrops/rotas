package com.rota.viagem.gateway.data.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class ResultadoRotaGateway {

    List<Integer> resultadoCaminho;
    Integer valor;
}
