package com.rota.viagem.gateway;

import com.rota.viagem.gateway.data.request.RotaGatewayRequest;

import java.io.IOException;

public interface RotaGateway {

    void inserirRegistroArquivo(RotaGatewayRequest gatewayRequest) throws IOException;

}
