package com.rota.viagem.controller.data.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RotaConsultaResponse {

    private String resultado;
}
