package com.rota.viagem.controller.data.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
public class RotaRequest {

    private String rotaDe;
    private String rotaPara;
    private BigDecimal valor;
}
