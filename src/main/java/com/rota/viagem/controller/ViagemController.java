package com.rota.viagem.controller;

import com.rota.viagem.controller.data.request.RotaRequest;
import com.rota.viagem.controller.data.response.RotaConsultaResponse;
import com.rota.viagem.entity.RotaConsultaEntity;
import com.rota.viagem.entity.RotaEntity;
import com.rota.viagem.usecase.RotaConsultaUseCase;
import com.rota.viagem.usecase.RotaUseCase;
import com.rota.viagem.converter.RotaConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rota")
public class ViagemController {

    private final RotaConverter rotaConverter;
    private final RotaUseCase rotaUseCase;
    private final RotaConsultaUseCase rotaConsultaUseCase;

    @PostMapping("/inserir")
    @ResponseBody
    public ResponseEntity inserirRota(@RequestBody RotaRequest rotaRequest) throws IOException {

            RotaEntity entity = rotaConverter.converterRotaRequestToEntity(rotaRequest);
            rotaUseCase.inserirNovaRota(entity);

            return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/consulta")
    @ResponseBody
    public ResponseEntity<RotaConsultaResponse> consultarRota(@RequestHeader  String rotaDe,
                                        @RequestHeader String rotaPara) throws IOException {

        RotaConsultaEntity entity = rotaConverter.converterRotaConsultaRequestToEntity(rotaDe, rotaPara);

        RotaConsultaResponse response = rotaConsultaUseCase.realizarConsulta(entity);

        return ResponseEntity.ok(response);

    }
}
