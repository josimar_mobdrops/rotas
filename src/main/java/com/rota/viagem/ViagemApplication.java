package com.rota.viagem;

import com.rota.viagem.controller.data.response.RotaConsultaResponse;
import com.rota.viagem.entity.RotaConsultaEntity;
import com.rota.viagem.usecase.impl.RotaConsultaUseCaseImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.util.Scanner;

@SpringBootApplication(scanBasePackages = {"com.rota"})
public class ViagemApplication {

	public static void main(String[] args) throws IOException {
		ConfigurableApplicationContext context = SpringApplication.run(ViagemApplication.class, args);
		iniciar(context);
	}
	private static void iniciar(ConfigurableApplicationContext context) throws IOException {

		RotaConsultaUseCaseImpl teste = (RotaConsultaUseCaseImpl) context.getBean("rotaConsultaUseCaseImpl");
		Scanner leitor = new Scanner(System.in);
		String origem = new String();
		String destino = new String();
		System.out.println("please enter the route:");
		String rota = leitor.nextLine();
		if(rota.length() < 7 || rota.length() > 7) {
			System.out.println("please enter with example format: XXX-YYY");

		}else{
			origem = rota.substring(0, 3);
			destino = rota.substring(4, 7);
		}

		RotaConsultaEntity entity = converterParaEntity(origem, destino);

		RotaConsultaResponse response = teste.realizarConsulta(entity);

		System.out.println("Best Route: " +response.getResultado());
	}

	private static RotaConsultaEntity converterParaEntity(String origem, String destino) {

		return RotaConsultaEntity.builder()
				.rotaDe(origem)
				.rotaPara(destino)
				.build();
	}

}
