package com.rota.viagem.utils;

import com.rota.viagem.entity.enumerador.RotaEnum;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class Utils {

    private static final String GRU = "GRU";
    private static final String BRC = "BRC";
    private static final String SCL = "SCL";
    private static final String CDG = "CDG";
    private static final String ORL = "ORL";

    public static int converterStringParaInteger(String rota) {
        Integer id = 0;
        switch (rota){

            case BRC:
                id = RotaEnum.BRC.getCodigo();
                break;

            case GRU:
                id = RotaEnum.GRU.getCodigo();
                break;

            case SCL:
                id = RotaEnum.SCL.getCodigo();
                break;

            case CDG:
                id = RotaEnum.CDG.getCodigo();
                break;

            case ORL:
                id = RotaEnum.ORL.getCodigo();
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + rota);
        }

        return id;
    }

    public static String converterIdsParaEscrita(Integer rota) {
        String escrita = new String();
        switch (rota){

            case 2:
                escrita = RotaEnum.BRC.getDesc();
                break;

            case 1:
                escrita = RotaEnum.GRU.getDesc();
                break;

            case 3:
                escrita = RotaEnum.SCL.getDesc();
                break;

            case 4:
                escrita = RotaEnum.CDG.getDesc();
                break;

            case 5:
                escrita = RotaEnum.ORL.getDesc();
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + rota);
        }

        return escrita;
    }
}
