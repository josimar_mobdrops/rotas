package com.rota.viagem.converter;

import com.rota.viagem.controller.data.request.RotaRequest;
import com.rota.viagem.controller.data.response.RotaConsultaResponse;
import com.rota.viagem.entity.RotaConsultaEntity;
import com.rota.viagem.entity.RotaEntity;
import com.rota.viagem.gateway.data.request.RotaConsultaGatewayRequest;
import com.rota.viagem.gateway.data.request.RotaGatewayRequest;
import com.rota.viagem.gateway.data.response.ResultadoRotaGateway;
import com.rota.viagem.gateway.data.response.RotaConsultaGatewayResponse;

import java.util.List;

public interface RotaConverter {

    RotaEntity converterRotaRequestToEntity(RotaRequest request);
    RotaGatewayRequest converterRotaEntityToGateway(RotaEntity entity);

    RotaConsultaEntity converterRotaConsultaRequestToEntity(String rotaDe, String rotaPara);
    RotaConsultaGatewayRequest converterRotaConsultaEntityToGateway(RotaConsultaEntity entity);

    RotaConsultaGatewayResponse converterRotaConsultaGatewayToRotaConsultaResponse(ResultadoRotaGateway retornoTrafego);

}
