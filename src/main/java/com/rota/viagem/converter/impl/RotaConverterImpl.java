package com.rota.viagem.converter.impl;

import com.rota.viagem.controller.data.request.RotaRequest;
import com.rota.viagem.controller.data.response.RotaConsultaResponse;
import com.rota.viagem.converter.RotaConverter;
import com.rota.viagem.entity.RotaConsultaEntity;
import com.rota.viagem.entity.RotaEntity;
import com.rota.viagem.gateway.data.request.RotaConsultaGatewayRequest;
import com.rota.viagem.gateway.data.request.RotaGatewayRequest;
import com.rota.viagem.gateway.data.response.ResultadoRotaGateway;
import com.rota.viagem.gateway.data.response.RotaConsultaGatewayResponse;
import com.rota.viagem.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class RotaConverterImpl implements RotaConverter {

    @Override
    public RotaEntity converterRotaRequestToEntity(RotaRequest request) {
        return RotaEntity.builder()
                .rotaDe(request.getRotaDe())
                .rotaPara(request.getRotaPara())
                .valor(request.getValor())
                .build();
    }

    @Override
    public RotaGatewayRequest converterRotaEntityToGateway(RotaEntity entity) {
        return RotaGatewayRequest.builder()
                .rotaDe(entity.getRotaDe())
                .rotaPara(entity.getRotaPara())
                .valor(entity.getValor())
                .build();
    }

    @Override
    public RotaConsultaEntity converterRotaConsultaRequestToEntity(String rotaDe, String rotaPara) {
        return RotaConsultaEntity.builder()
                .rotaDe(rotaDe)
                .rotaPara(rotaPara)
                .build();
    }

    @Override
    public RotaConsultaGatewayRequest converterRotaConsultaEntityToGateway(RotaConsultaEntity entity) {
        return RotaConsultaGatewayRequest.builder()
                .partida(Utils.converterStringParaInteger(entity.getRotaDe()))
                .destino(Utils.converterStringParaInteger(entity.getRotaPara()))
                .build();
    }

    @Override
    public RotaConsultaGatewayResponse converterRotaConsultaGatewayToRotaConsultaResponse(ResultadoRotaGateway trafegoMaisCurto) {
        return RotaConsultaGatewayResponse.builder()
                .caminho(converterListaResultado(trafegoMaisCurto.getResultadoCaminho()))
                .valor(trafegoMaisCurto.getValor())
                .build();
    }

    private List<String> converterListaResultado(List<Integer> trafegos){

        List<String> responseList = new ArrayList<>();

        trafegos.stream().forEach(x ->
                responseList.add(Utils.converterIdsParaEscrita(x)));

        return responseList;
    }
}
