package com.rota.viagem.entity;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RotaConsultaEntity {

    private String rotaDe;
    private String rotaPara;
}
