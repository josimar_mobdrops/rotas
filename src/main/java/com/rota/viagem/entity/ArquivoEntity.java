package com.rota.viagem.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.beanio.annotation.Field;
import org.beanio.annotation.Record;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Record
public class ArquivoEntity {

    @Field(at = 0, length = 3)
    private String origemRota;

    @Field(at = 4, length = 3)
    private String destinoRota;

    @Field(at = 8, length = 2,maxLength = 2)
    private String valor;

}
