package com.rota.viagem.entity.enumerador;

import lombok.Getter;

@Getter
public enum RotaEnum {

    GRU(1, "GRU"),
    BRC(2, "BRC"),
    SCL(3, "SCL"),
    CDG(4, "CDG"),
    ORL(5, "ORL");

    private int codigo;
    private String desc;

    RotaEnum(int id, String descricao) {
        codigo = id;
        desc = descricao;
    }
}
