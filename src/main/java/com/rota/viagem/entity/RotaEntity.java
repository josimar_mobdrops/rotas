package com.rota.viagem.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;



@Builder
@Getter
public class RotaEntity {

    private String rotaDe;
    private String rotaPara;
    private BigDecimal valor;
}
