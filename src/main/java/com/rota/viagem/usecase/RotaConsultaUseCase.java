package com.rota.viagem.usecase;

import com.rota.viagem.controller.data.response.RotaConsultaResponse;
import com.rota.viagem.entity.RotaConsultaEntity;

import java.io.IOException;

public interface RotaConsultaUseCase {

    RotaConsultaResponse realizarConsulta(RotaConsultaEntity entity) throws IOException;
}
