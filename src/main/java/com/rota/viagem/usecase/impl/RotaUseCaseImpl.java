package com.rota.viagem.usecase.impl;

import com.rota.viagem.controller.data.response.RotaConsultaResponse;
import com.rota.viagem.entity.RotaConsultaEntity;
import com.rota.viagem.entity.RotaEntity;
import com.rota.viagem.gateway.RotaGateway;
import com.rota.viagem.gateway.data.request.RotaGatewayRequest;
import com.rota.viagem.usecase.RotaUseCase;
import com.rota.viagem.converter.RotaConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class RotaUseCaseImpl implements RotaUseCase {

    private final RotaGateway rotaGateway;

    @Qualifier("RotaGatewayConverter")
    private final RotaConverter converter;


    @Override
    public void inserirNovaRota(RotaEntity rotaEntity) throws IOException {

        RotaGatewayRequest gatewayRequest = converter.converterRotaEntityToGateway(rotaEntity);

        rotaGateway.inserirRegistroArquivo(gatewayRequest);

    }
}
