package com.rota.viagem.usecase.impl;

import com.rota.viagem.gateway.data.response.ResultadoRotaGateway;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@RequiredArgsConstructor
@AllArgsConstructor
public class Graph {
    private static final int UNDEFINED = -1;
    private int vertices[][];

    public Graph(int colunaVertice, int linhaVertice) {
        vertices = new int[colunaVertice][linhaVertice];
    }

    public void tracarEscala(int partida, int destino, int valor) {
        vertices[partida][destino] = valor;
        vertices[destino][partida] = valor;
    }

    public void removerEscala(int partida, int destino) {
        vertices[partida][destino] = 0;
        vertices[destino][partida] = 0;
    }

    public int getValor(int partida, int destino) {
        return vertices[partida][destino];
    }

    /**
     * @param vertice Origin vertex
     * @return a list with the index of all vertexes connected to the given vertex.
     */
    public List<Integer> getNoVizinho(int vertice) {
        List<Integer> vizinho = new ArrayList<>();
        for (int i = 0; i < vertices[vertice].length; i++)
            if (vertices[vertice][i] > 0) {
                vizinho.add(i);
            }

        return vizinho;
    }

    /**
     * Implementation of the Dijkstra's algorithm.
     * @param from Source node
     * @param to Destionation node
     * @return The path.
     */
    public ResultadoRotaGateway path(int from, int to) {
        //Initialization
        //--------------
        int cost[] = new int[vertices.length];
        int prev[] = new int[vertices.length];
        Set<Integer> unvisited = new HashSet<>();

        //The initial node has cost 0 and no previous vertex
        cost[from] = 0;

        //All other nodes will have its cost set to MAXIMUM and undefined previous
        for (int v = 0; v < vertices.length; v++) {
            if (v != from) {
                cost[v] = Integer.MAX_VALUE;
            }
            prev[v] = UNDEFINED;
            unvisited.add(v);
        }

        //Graph search
        //------------
        while (!unvisited.isEmpty()) {
            int near = closest(cost, unvisited);
            unvisited.remove(near);

            for (Integer neighbor : getNoVizinho(near)) {
                int totalCost = cost[near] + getValor(near, neighbor);
                if (totalCost < cost[neighbor]) {
                    cost[neighbor] = totalCost;
                    prev[neighbor] = near;
                }
            }
            //Found?
            if (near == to) {
                return makePathList(prev, near, cost[near]);
            }
        }

        //No path found
        return ResultadoRotaGateway.builder().build();
    }

    private int closest(int[] dist, Set<Integer> unvisited) {
        double minDist = Integer.MAX_VALUE;
        int minIndex = 0;
        for (Integer i : unvisited) {
            if (dist[i] < minDist) {
                minDist = dist[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    private ResultadoRotaGateway makePathList(int[] prev, int u, int cost) {

        List<Integer> path = new ArrayList<>();
        path.add(u);

        while (prev[u] != UNDEFINED) {
            path.add(prev[u]);
            u = prev[u];
        }
        Collections.reverse(path);

        return ResultadoRotaGateway.builder()
                .resultadoCaminho(path)
                .valor(cost)
                .build();
    }
}
