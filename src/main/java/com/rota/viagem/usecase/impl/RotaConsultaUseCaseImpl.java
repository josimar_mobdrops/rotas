package com.rota.viagem.usecase.impl;

import com.rota.viagem.controller.data.response.RotaConsultaResponse;
import com.rota.viagem.converter.RotaConverter;
import com.rota.viagem.entity.ArquivoEntity;
import com.rota.viagem.entity.RotaConsultaEntity;
import com.rota.viagem.gateway.RotaConsultaGateway;
import com.rota.viagem.gateway.data.request.RotaConsultaGatewayRequest;
import com.rota.viagem.gateway.data.response.RotaConsultaGatewayResponse;
import com.rota.viagem.usecase.RotaConsultaUseCase;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.beanio.BeanReader;
import org.beanio.StreamFactory;
import org.beanio.builder.FixedLengthParserBuilder;
import org.beanio.builder.StreamBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
@Log
public class RotaConsultaUseCaseImpl implements RotaConsultaUseCase {

    private final RotaConsultaGateway rotaConsultaGateway;
    private final RotaConverter rotaConverter;

    @Override
    public RotaConsultaResponse realizarConsulta(RotaConsultaEntity entity) throws IOException {

        RotaConsultaGatewayRequest gatewayRequest = rotaConverter.converterRotaConsultaEntityToGateway(entity);

        File arquivo = buscarArquivo();
        List<ArquivoEntity> arquivoEntityList = lerArquivo(arquivo);

        RotaConsultaGatewayResponse response = rotaConsultaGateway.executar(gatewayRequest, arquivoEntityList);

        StringBuilder resultado = new StringBuilder();
            response.getCaminho().stream()
                    .forEach(x -> resultado.append((x + " - ")));

        return RotaConsultaResponse.builder()
                .resultado(resultado.toString() + "> " + response.getValor())
                .build();
    }


    private List<ArquivoEntity> lerArquivo(File arquivo) {
        StreamFactory factory = StreamFactory.newInstance();
        factory.define(new StreamBuilder("inputVariables")
                .format("fixedlength")
                .parser(new FixedLengthParserBuilder())
                .addRecord(ArquivoEntity.class));
        BeanReader in = factory.createReader("inputVariables", arquivo);

        ArquivoEntity arquivoEntity = (ArquivoEntity) in.read();

        List<ArquivoEntity> listArquivoEntity = new ArrayList<>();
        listArquivoEntity.add(arquivoEntity);
        for (int x = 0; in.getLineNumber()> x; x++){

            arquivoEntity = (ArquivoEntity) in.read();

            listArquivoEntity.add(arquivoEntity);

        }
        return  listArquivoEntity;
    }

    private File buscarArquivo() throws FileNotFoundException {
        String path = System.getProperty("input.file.name");
        final boolean hasPath = (StringUtils.isEmpty(path));
        final boolean hasFilePath = (!Paths.get(path).toFile().exists());
        if(hasPath){
            log.severe("ERRO: Variável -Dinput.file.name não definida");
            throw new FileNotFoundException("Path Not Found!");
        }else if(hasFilePath){
            log.severe("ERRO: Arquivo não encontrado!");
            throw new FileNotFoundException("File Not Found!");
        }

        return Paths.get(path).toFile();
    }
}
