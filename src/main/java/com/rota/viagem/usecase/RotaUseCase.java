package com.rota.viagem.usecase;

import com.rota.viagem.entity.RotaEntity;

import java.io.IOException;

public interface RotaUseCase {

    void inserirNovaRota(RotaEntity rotaEntity) throws IOException;

}
