Como Executar a aplicação:
    -Dinput.file.name=<Caminho Arquivo>\input-file.txt
-- Exemplo:
    -Dinput.file.name=C:\Users\josimar\Documents\BancoBexs\input-file.txt

Estrutura de pacotes:
    O sistema foi desenvolvido utilizando a Arquitetura Clean, separando suas camadas em:
        Controller  -> Classes controladoras (Interface) de contratos de entrada
        Use Case    -> Classes responsáveis por toda a regra de negócio da nossa aplicação
        Gateway     -> Classes responsáveis pelas chamadas Externas a aplicação, como neste caso, ela faz a orquestração do consumo do algoritmo.
        Converter   -> Diretório Obtém a responsabilidade de realizar as conversões de DTO entre as camadas (Controller para use case, da use case para gateway e seus respectivos retornos);
        Entity      -> Alocado todos os objetos de negócio da aplicação, utilizados somente pela Use Case
        Utils       -> Criado para Criar métodos que possam suportar a aplicação como um todo, a fim de otimizar a reutilização de métodos.
Nos diretórios Controller e Gateway, podemos encontrar o diretório Data:
        Data / Request  -> Criado com o intuíto de separar os DTOs de Entrada de cada camada;
        Data / Response -> Criado com o intuíto de separar os DTOs de Retorno de cada camada; 

Na Classe Main, se encontra a interface de console reutilizando toda a arquitetura descrita e realizando a consulta.

Interface Rest:

Url: http://localhost:5000/rota/consulta
parametros de entrada:
Header: rotaDe
Header: rotaPara

Url: http://localhost:5000/rota/inserir
Body: 
{
	"rotaDe": "GRU",
	"rotaPara": "TRE",
	"valor": 10
}
        